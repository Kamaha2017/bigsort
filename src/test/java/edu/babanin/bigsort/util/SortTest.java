package edu.babanin.bigsort.util;

import java.util.Arrays;
import java.util.Date;
import java.util.function.Function;

import static org.junit.Assert.assertArrayEquals;

public abstract class SortTest {
    protected static final int MAX_VALUE = 1_000_000_000 + 1;
    protected static final int MAX_SIZE = 100_000_000;

    void checkSort(String name, Function<int[], int[]> sort){
        int[] ints = Generate.gen(1_000_000, 10_000);
        int[] copyInts = Arrays.copyOf(ints, ints.length);

        int[] intsSorted = Arrays.stream(ints).parallel().sorted().toArray();
        copyInts = sort.apply(copyInts);
        assertArrayEquals(intsSorted, copyInts);
    }

    void timeSort5Avr(String name, Function<int[], int[]> sort){
        long timeAvarage = 0;

        for (int i = 0; i < 5; i++) {
            long time = new Date().getTime();

            int[] ints = Generate.gen(MAX_VALUE, MAX_SIZE);

            long time2 = new Date().getTime();

            sort.apply(ints);

            long time3 = new Date().getTime();

            timeAvarage += time3 - time2;
        }

        timeAvarage /= 5;

        System.out.println("sort name: " + name);
        System.out.println("sort time: " + timeAvarage + " ms");
    }
}
