package edu.babanin.bigsort.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.*;

public class ParallelQuickSortTest  extends SortTest{
    private static final int MAX_VALUE = 1_000_000_000 + 1;
    private static final int MAX_SIZE = 100_000_000;

    /**
     * Проверка на валидность полученных результатов
     *
     * @throws Exception
     */
    @Test
    public void sort() throws Exception {
        checkSort("paralell quick sort", (ints) -> {
            new ParallelQuickSort().sort(ints);
            return ints;
        });
    }

    /**
     * Надуманный тест, чтобы посмотреть время выполнения
     *
     * @throws Exception
     */
    @Test
    public void time() throws Exception {
        timeSort5Avr("paralell quick sort", (ints) -> {
            new ParallelQuickSort().sort(ints);
            return ints;
        });
    }
}