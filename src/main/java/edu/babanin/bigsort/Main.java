package edu.babanin.bigsort;

import edu.babanin.bigsort.util.AbstractQuickSort;
import edu.babanin.bigsort.util.Generate;
import edu.babanin.bigsort.util.ParallelQuickSort;

import java.io.IOException;

public class Main {
    private static final int MAX_VALUE = 1_000_000_000 + 1;
    private static final int MAX_SIZE = 100_000_000;

    public static void main(String[] args) throws IOException {
        int[] ints = Generate.gen(MAX_VALUE, MAX_SIZE);

        AbstractQuickSort quickSort = new ParallelQuickSort();
        quickSort.sort(ints);

        for (int i = 0; i < MAX_SIZE; i += 1_000_000) {
            System.out.println(ints[i]);
        }

        System.out.println("");
        System.out.println("ints[0]=" + ints[0] + "  ints[" + (ints.length - 1) + "]=" + ints[ints.length - 1]);
    }
}
