package edu.babanin.bigsort.util;

/**
 * Абстрактный класс
 */
public abstract class AbstractQuickSort {
    public abstract void sort(int[] a);

    protected int partition(int[] a, int lo, int hi) {
        int i = lo, j = hi + 1;
        int v = a[lo];
        while (true) {
            while (less(a[++i], v)) if (i == hi) break;
            while (less(v, a[--j])) if (j == lo) break;
            if (i >= j ) break;
            exch(a, i, j);
        }

        exch(a, lo, j);
        return j;
    }

    private void exch(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    private boolean less(int i, int v) {
        return i < v;
    }
}
